# Enterprise Directory .NET Clients

A collection of clients, targeting Microsoft's .NET standard framework version 2.0, for connecting to Virginia Tech's Enterprise Directory. Currently supported protocols include ED-ID and the REST API.